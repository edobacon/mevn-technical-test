import express from "express";
import history from "connect-history-api-fallback-exclusions";
import mongoose from "mongoose";
import { resolve } from "path";
import morgan from 'morgan';
import errorMiddleware from "./middlewares/error.middleware.js";
import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';

class App {
  constructor(port, controllers = []) {
    this.app = express();
    this.port = port;
    this.mongoUri = `${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_NAME}`;
    this.connection = mongoose.connection;
    this.dbOptions = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
    };
    if (Number(process.env.MONGO_AUTH_REQUIRED)) {
      this.dbOptions.user = process.env.MONGO_USERNAME;
      this.dbOptions.pass = process.env.MONGO_PASSWORD;
      this.dbOptions.authSource = process.env.MONGO_AUTH;
    }
    this.initializaSwagger();
    this.initializeMiddlewares();
    this.initializeDb();
    this.initializeControllers(controllers);
    this.initializeStaticFiles();
    this.errorHandler();
  }
  initializeMiddlewares() {
    this.app.use(morgan('dev'));
    this.app.use(history({
      exclusions: [
        '/api/*',
      ]
    }));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', [process.env.CLIENT_DOMAIN]);
      res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
      res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
      res.header('Allow', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
      next();
  });
  }
  initializeDb() {
    mongoose.connect(this.mongoUri, this.dbOptions);
    this.connection.once("open", () => {
      console.log("Mongodb Connected!");
    });
    this.connection.on("error", (err) => {
      console.log(`Mongodb connection error: ${err.message}`);
      process.exit();
    });
  }
  initializeStaticFiles() {
    this.app.use(express.static("client/dist"));
    this.app.get("*", (req, res) => {
      res.sendFile(resolve(__dirname, "client", "dist", "index.html"));
    });
  }
  initializaSwagger() {
    const swaggerOptions = {
      swaggerDefinition: {
        info: {
          title: 'MEVN Technical Test',
          description: 'Development of a MEVN solution for a technical test',
          contact: {
            name: 'Eduardo Bacon'
          },
          servers: [{ url: 'http://localhost:3000', description: 'Development server' }]
        }
      },
      apis: ['./controllers/*.controller.js']
    };
    const swaggerDocs = swaggerJsDoc(swaggerOptions);
    this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))
  }
  initializeControllers(controllers) {
    controllers.forEach((controller) => {
      this.app.use('/api', controller.router);
    });
  }
  errorHandler() {
    this.app.patch('*', (req, res) => res.status(404).json({ errors: 'Not Implemented' }));
    this.app.post('*', (req, res) => res.status(404).json({ errors: 'Not Implemented' }));
    this.app.delete('*', (req, res) => res.status(404).json({ errors: 'Not Implemented' }));
    this.app.use(errorMiddleware);
  }
  listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on port ${this.port}`);
    });
  }
}

export default App;
