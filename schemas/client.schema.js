import mongoose from 'mongoose';

const clientSchema = new mongoose.Schema({
  name: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  },
  providers: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Provider',
    default: []
  }
});

export default mongoose.model('Client', clientSchema);