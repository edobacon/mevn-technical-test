import mongoose from 'mongoose';

const providerSchema = new mongoose.Schema({
  name: {
    type: String
  }
});

export default mongoose.model('Provider', providerSchema);