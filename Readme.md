# MEVN Technical Test

Development of a fullstack solution for a client and providers simple administration, the project is developed using the Mongo, Express, Vue, Node Stack.
Before start any test, is required to set the enviroment variables on client and server and load data to mongodb.
You must have mongodb installed on your device.

Frontend developed with Vue 3, Vue-Router 4, Tailwind 2, FontAwesome & Axios.
Backend developed with Express & Mongoose.

**Guide for a quick first start local server**:

* Populate MongoDb Run: mongorestore {folder} **You must change {folder} for mongodump folder location.**
* Set Server enviroments
* Set Client enviroments
* Run *"npm run full-start"* on server location folder

## Enviroments
You must add .env variables before start the server or the client

#### Server Enviroment
locatedon: /.env

>**API_PORT**
Set the port to use in the server

>**CLIENT_DOMAIN**
Set the domain of the client side to allow cors
*example: http://localhost:8080*

>**MONGO_URI**
Set the uri to connect to the Mongo Database
*example: mongodb://localhost*

>**MONGO_PORT**
Set the port to connect to the Mongo Database
*example: 27017*

>**MONGO_NAME**
Set the name of the project
*example: mevn*

>**MONGO_AUTH_REQUIRED**
Set if auth is required to connect to the Mongo Database
*example 0 for not need, 1 for required*

>**MONGO_AUTH**
Set the mongo user collection where to ask Mongo Database Permission
*example: auth*

>**MONGO_USERNAME**
Set the username for Mongo Database admin login
*example: root*

>**MONGO_PASSWORD**
Set the password for Mongo Database admin login
*example: root*

#### Client Enviroments
located on: /client/.env

>**VUE_APP_SERVER_DOMAIN**
Set the server domain to use on client side for api calls
*example: http://localhost*

>**VUE_APP_SERVER_PORT**
Set the server port to use on client side for api calls
*example: 3000*

## Database
Mongodump files propilate mongo database "MEVN" with some example providers.

You must have mongo daemon running.
Then you can run:
mongorestore {folder}
You must change {folder} for mongodump folder location.

## Scripts

>**npm run dev-client**
Starts a webpack server for development with auto reloading on code changes on the client code.

>**npm run dev-server**
Starts a local nodemon server for development with auto reloading on code changes on the server code.

>**npm run pre-build**
Build client side project and npm dependency installs for production use, must be used before start production a server to update client code.

>**npm start**
Start a server for production use.

>**npm run full-start**
Install dependencies on server and client, build the client and start the production server.

**Examples:**
>For Development use, you need to Terminal/Console, one to keep client auto-reload and one for server auto-reload.pointer-events-none
**Terminal 1:**
**Run:** npm run dev-client
**Terminal 2:**
**Run:** npm run dev-server

>For production use, you need to build the client first, if there are any changes, then you can start server. 
The server will provide access to client files and API.
**Run:**
npm run pre-build && npm start

>If there are no changes on the client code, you can skip client build and just start the server, serving the client files and providing access to API.
**Run:**
npm start

## Docs
Server Endpoints are documented with swagger,you can read the documentation on /api-docs once the server is running on dev or production.

### What's next?
Enable separated production and development enviroments.
Check and update npm dependencies.


### Example envs

**server**
Located on /.env

>API_PORT=3000
>
>CLIENT_DOMAIN=http://localhost:8080
>
>MONGO_URI=mongodb://localhost
>
>MONGO_PORT=27017
>
>MONGO_NAME=mevn
>
>MONGO_AUTH_REQUIRED=0
>
>MONGO_AUTH=auth
>
>MONGO_USERNAME=root
>
>MONGO_PASSWORD=root

**client**
Located on /client/.env

>VUE_APP_SERVER_DOMAIN=http://localhost
>
>VUE_APP_SERVER_PORT=3000



