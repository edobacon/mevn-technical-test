import { computed, ref } from "vue";

const state = ref(false);
const clr = ref('#9BA09B');
const msg = ref('Toast Message !');
const toastStatus = computed(() => ({ state: state.value, color: clr.value, message: msg.value }));
const setToastStatus = (status, color, message) => {
  msg.value = message;
  switch (color) {
    case 'success':
      clr.value = '#1B9B11';
      break;
    case 'error':
      clr.value = '#B01010';
      break;
    case 'alert':
      clr.value = '#D3D32F';
      break;
    case 'info':
      clr.value = '#1684CB';
      break;
    default:
      return '#9BA09B';
  }
  state.value = status;
  if (status) {
    setTimeout(() => state.value = false, 4000);
  }

}


export {
  toastStatus,
  setToastStatus
}