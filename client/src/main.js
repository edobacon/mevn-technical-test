import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './css/tailwind.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faTimes,
  faCheck,
  faTrash,
  faEdit
} from '@fortawesome/free-solid-svg-icons';
library.add(faTimes, faCheck, faTrash, faEdit);

const app = createApp(App);
app.component('fa-icon', FontAwesomeIcon);
app.use(router).mount('#app');