import { createRouter, createWebHistory } from 'vue-router'
import Clients from '../views/Clients.vue'

const routes = [
  {
    path: '/',
    name: 'Clients',
    component: Clients
  },
  {
    path: "/:pathMatch(.*)*",
    redirect: '/'
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
