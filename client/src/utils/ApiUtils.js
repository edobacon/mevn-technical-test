import axios from 'axios';

axios.defaults.baseURL = `${process.env.VUE_APP_SERVER_DOMAIN}:${process.env.VUE_APP_SERVER_PORT}/api/`;

const METHODS = {
  GET: "GET",
  POST: "POST",
  PATCH: "PATCH",
  DELETE: "DELETE",
};

export async function makeRequest(method, url, body = {}) {
  try {
    const requestOpts = {
      method: METHODS[method],
      url,
      data: JSON.parse(JSON.stringify(body))
    }
    const response = await axios(requestOpts);
    const { status, data } = response;
    return { ok: true, status: status, payload: data.payload };
  } catch (error) {
    const { status, data } = error.response
    return { ok: false, status, errors: data.errors  }
  }
}