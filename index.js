import dotenv from 'dotenv';
dotenv.config();
import App from './app.js';
import ClientController from './controllers/client.controller.js';
import ProviderController from './controllers/provider.controller.js';

const app = new App(process.env.API_PORT || 5000, [ new ClientController(), new ProviderController() ])
app.listen();