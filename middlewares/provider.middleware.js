import mongoose from 'mongoose';
import { stringValidator } from "../helpers/validators.helper.js";
import { HttpException } from "./error.middleware.js";

export function validateProviderPostData(req, res, next) {
  const { name } = req.body;
  const [ error, code, message ] = stringValidator(name, 'Provider');
  if (error) return next(new HttpException(code, message));
  next();
}

export function mustIncludeProviderIdOnParam(req, res, next) {
  const { id } = req.params
  if (!id) return next(new HttpException(400, "You Must include a ProviderId"));
  if (!mongoose.Types.ObjectId.isValid(id)) return next(new HttpException(400, "ProviderId must be a valid ObjectId"));
  next();
}

export function validateUpdateProviderData(req, res, next) {
  const { name } = req.body;
  if (!name) return next(new HttpException(400, "Nothing to update"));
  const [ error, code, message ] = stringValidator(name, 'Provider');
  if (error) return next(new HttpException(code, message));
  next();
}