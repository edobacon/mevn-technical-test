export class HttpException extends Error {
  constructor(status = 500, errors = []) {
    super();
    this.status = status || 500;
    this.errors = errors || "Oh!, Something went wrong!";
  }
}

function errorMiddleware(error, req, res, next) {
  console.log({error});
  const status = error.status || 500;
  const errors = error.errors || "Oh!, Something went wrong!";
  res.status(status).send({ errors });
}

export default errorMiddleware;
