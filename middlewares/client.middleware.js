import { HttpException } from "./error.middleware.js";
import mongoose from 'mongoose';
import { emailFormatValidator, stringLengthValidator, stringValidator } from "../helpers/validators.helper.js";
import Client from '../schemas/client.schema.js';

export function validateClientPostData(req, res, next) {
  const { name, email, phone, providers } = req.body;
  const errors = [];
  const [ nameError, , nameErrorMessage ] = stringValidator(name, "Name");
  nameError && errors.push(nameErrorMessage);
  const [ emailError, , emailErrorMessage ] = stringValidator(email, "Email");
  emailError && errors.push(emailErrorMessage);
  const [ phoneError, , phoneErrorMessage ] = stringValidator(phone, "Phone");
  phoneError && errors.push(phoneErrorMessage);
  if (errors.length > 0) return next(new HttpException(400, errors));
  //
  const [ emailFormatError, emailFormatCode, emailFormatErrorMessage ] = emailFormatValidator(email, "Email");
  if (emailFormatError) return next(new HttpException(emailFormatCode, emailFormatErrorMessage));
  //
  const [ phoneLengthError, phoneLengthCode, phoneLengthErrorMessage ] = stringLengthValidator(phone, "Phone", 9, 9);
  if (phoneLengthError) return next(new HttpException(phoneLengthCode, phoneLengthErrorMessage));
  //
  if (!Array.isArray(providers)) return next(new HttpException(400, "Invalid Provider Parameter, must be an Array"));
  if (providers && providers.length > 0) {
    const validIds = providers.map(el => mongoose.Types.ObjectId.isValid(el));
    if (validIds.some(el => !el)) return next(new HttpException(400, "Provider must be an Array of valid ObjectIds"));
  }
  next();
}

export async function isUniqueEmailValidator(req, res, next) {
  const { email } = req.body;
  const client = await Client.find({ email });
  if (client.length > 0) return next(new HttpException(400, 'Email already in use'));
  next();
}

export function validateUpdateClientData(req, res, next) {
  const { name, email, phone, providers } = req.body;
  if (!name && !email && !phone && !providers) return next(new HttpException(400, "Nothing to update"));
  const errors = [];
  if (name) {
    const [ nameError, , nameErrorMessage ] = stringValidator(name, "Name");
    nameError && errors.push(nameErrorMessage);
  }
  if (email) {
    const [ emailError, , emailErrorMessage ] = stringValidator(email, "Email");
    emailError && errors.push(emailErrorMessage);
    const [ emailFormatError, emailFormatCode, emailFormatErrorMessage ] = emailFormatValidator(email, "Email");
    if (emailFormatError) return next(new HttpException(emailFormatCode, emailFormatErrorMessage));
  }
  if (phone) {
    const [ phoneError, , phoneErrorMessage ] = stringValidator(phone, "Phone");
    phoneError && errors.push(phoneErrorMessage);
    const [ phoneLengthError, phoneLengthCode, phoneLengthErrorMessage ] = stringLengthValidator(phone, "Phone", 9, 9);
    if (phoneLengthError) return next(new HttpException(phoneLengthCode, phoneLengthErrorMessage));
  }
  if (errors.length > 0) return next(new HttpException(400, errors));
  if (providers) {
    if (!Array.isArray(providers)) return next(new HttpException(400, "Invalid Provider Parameter, must be an Array"));
    if (providers.length > 0) {
      const validIds = providers.map(el => mongoose.Types.ObjectId.isValid(el));
      if (validIds.some(el => !el)) return next(new HttpException(400, "Provider must be an Array of valid ObjectIds"));
    }
  }
  next();
}

export async function isUniqueAndOwnerEmailValidator(req, res, next) {
  const { email } = req.body
  if (!email) return next();
  //
  const { id } = req.params;
  const client  = await Client.findById(id);
  if (!client) return next(new HttpException(404, "Client not Found"));
  const clientWithEqualEmail = await Client.find({ email });
  if (clientWithEqualEmail.length > 1) return next(new HttpException(400, "Too many email matches"));
  if (clientWithEqualEmail.length === 1 && clientWithEqualEmail[0].id !== id) return next(new HttpException(400, "Email does not correspond to the Client"));
  next();
}

export function mustIncludeClientIdOnParam(req, res, next) {
  const { id } = req.params
  if (!id) return next(new HttpException(400, "You Must include a ClientId"));
  if (!mongoose.Types.ObjectId.isValid(id)) return next(new HttpException(400, "ClientId must be a valid ObjectId"));
  next();
}
