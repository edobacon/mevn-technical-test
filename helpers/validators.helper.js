export function stringValidator(val, name = "Field") {
  if (!val) return [true, 400, `Must include a ${name}`];
  if (typeof val !== "string") return [true, 400, `${name} name must be a string`];
  if (val.trim().length === 0) return [true, 404, `${name} name can\'t be an empty string`];
  return [false, 200, ''];
}

export function stringLengthValidator(val, name = "Field", min = 0, max = 0) {
  if (min > 0 && max > 0 && (val.length < min || val.length > max)) return [true, 400, min === max ? `${name} must be ${min} characteres long` : `${name} must be between ${min} and ${max} characteres`];
  if (min > 0 && val.length < min) return [true, 400, `${name} must be at least ${min} characters long`];
  if (max > 0 && val.length > max) return [true, 400, `${name} must be no more than ${max} characters long`];
  return [false, 200, ''];
}

export function emailFormatValidator(val, name = "Field") {
  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const error = !pattern.test(val);
  return [error, error ? 400 : 200, error ? `${name} must be a valid Email` : ''];
}
